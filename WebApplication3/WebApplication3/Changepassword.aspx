﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Changepassword.aspx.cs" Inherits="WebApplication3.Changepassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="currentpassword" runat="server" Text="Current Password" Font-Bold="false" ForeColor="black"></asp:Label>
            <asp:textbox ID="current_password" runat="server" TextMode="Password"></asp:textbox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Current Password" ControlToValidate="current_password">
            </asp:RequiredFieldValidator>
            <br />

            <asp:Label ID="newpassword" runat="server" Text="New Password"></asp:Label>
            <asp:textbox ID="new_password" runat="server" TextMode="Password"></asp:textbox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter new password" ControlToValidate="new_password"></asp:RequiredFieldValidator>
            <br />

            <asp:Label ID="confirm_newpassword" runat="server" Text="Confirm Password"></asp:Label>
            <asp:textbox ID="confirm_password" runat="server" TextMode="Password"></asp:textbox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter confirm password" ControlToValidate="confirm_password"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Password Mismatch" ControlToCompare="new_password" ControlToValidate="confirm_password"></asp:CompareValidator>
          </div>
        <asp:Button ID="changepassword" runat="server" Text="Change Password" OnClick="changepassword_Click" />
        <asp:Label ID="label1" runat="server" Text=""></asp:Label>
            </form>
</body>

</html>
