﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;



namespace WebApplication3
{
	public partial class Welcome : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (Session["username"] != null && Session["email"]!=null)
            {
                Label1.Text = "WELCOME::" + Session["username"].ToString();
            }
            else
            {
                Response.Redirect("UserLogin2.aspx");
            }
		}

        protected void link_changepassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("Changepassword.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Session.Clear();
            Session.Abandon();
            Response.Redirect("UserLogin2.aspx");
        }
    }
}