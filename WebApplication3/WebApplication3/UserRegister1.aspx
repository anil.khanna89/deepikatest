﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserRegister1.aspx.cs" Inherits="WebApplication3.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="registration.css" rel="stylesheet" />
</head>
<body >
    
    <form id="form1" runat="server" >
        <div class="container">
            <asp:Label ID="Label1" runat="server" Text="Username" ForeColor="Black" ></asp:Label>
            <asp:TextBox ID="usertext" runat="server" ></asp:TextBox>
           <!-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Username must be filled" ControlToValidate="usertext" ForeColor="Red"></asp:RequiredFieldValidator> 
      -->
        
        <p>

            <asp:Label ID="Label2" runat="server" Text="Email"></asp:Label>
            <asp:TextBox ID="email" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Email must be filled" ControlToValidate="email" ForeColor="Red"></asp:RequiredFieldValidator>

            </p>
        <p>

            <asp:Label ID="Label3" runat="server" Text="Phone-Number"></asp:Label>
             <asp:TextBox ID="phone_number" runat="server"  ></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Phone_number must be filled" ControlToValidate="phone_number" ForeColor="Red"></asp:RequiredFieldValidator>
           
            </p>
        <p>
            
            <asp:Label ID="Label4" runat="server" Text="Password"></asp:Label>
            <asp:TextBox ID="password" runat="server" ></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Password must be filled" ControlToValidate="password" ForeColor="Red"></asp:RequiredFieldValidator>
            </p>
        <p>
            
            <asp:Label ID="Label5" runat="server" Text="Repeat-Password"></asp:Label>
            <asp:TextBox ID="repeat_password" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Repeat Password must be filled" ControlToValidate="repeat_password" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </p>
        <p>
            

            <asp:Button ID="reg_button" runat="server" Text="Register" OnClick="reg_button_Click" style="margin-top: 13px" />
        </p>

            <h2>Already have an Account?<a href="UserLogin2.aspx">Sign In </a></h2>
        </div>
    </form>
</body>
</html>
