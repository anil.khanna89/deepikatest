﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="WebApplication3.Employee" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Emp_Id"></asp:Label>
            <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
            <asp:Label ID="Label2" runat="server" Text="Emp_Salary"></asp:Label>
            <asp:TextBox ID="txtempsalary" runat="server"></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="Dept_Id"></asp:Label>
            <asp:TextBox ID="txtdeptid" runat="server"></asp:TextBox>
            <asp:Label ID="Label4" runat="server" Text="Dept_Name"></asp:Label>
            <asp:TextBox ID="txtdeptname" runat="server"></asp:TextBox>

            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />

        </div>
    </form>
</body>
</html>
