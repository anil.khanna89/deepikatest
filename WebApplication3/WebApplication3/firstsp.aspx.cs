﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace WebApplication3
{
    public partial class firstsp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
            SqlCommand comm = new SqlCommand("Submit", connection);
            comm.Parameters.Add("@username", SqlDbType.VarChar,50).Value = TextBox1.Text;
            comm.Parameters.Add("@email", SqlDbType.VarChar,50).Value = TextBox2.Text;
            comm.Parameters.Add("@phonenumber", SqlDbType.VarChar,50).Value = TextBox3.Text;
            comm.Parameters.Add("@password", SqlDbType.VarChar,50).Value = TextBox4.Text;
            comm.CommandType = CommandType.StoredProcedure;
            connection.Open();
            comm.ExecuteNonQuery();
            connection.Close();

        }
    }
}