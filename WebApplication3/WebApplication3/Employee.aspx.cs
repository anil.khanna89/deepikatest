﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication3
{
    public partial class Employee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
            SqlCommand comm = new SqlCommand("InsertUpdateDelete1", connection);
            comm.Parameters.Add("@Emp_Id", SqlDbType.VarChar, 50).Value =txtempid.Text;
            comm.Parameters.Add("@Emp_Sal", SqlDbType.VarChar, 50).Value = txtempsalary.Text;
            comm.Parameters.Add("@Dept_Id", SqlDbType.VarChar, 50).Value = txtdeptid.Text;
            comm.Parameters.Add("@Dept_Name", SqlDbType.VarChar, 50).Value = txtdeptname.Text;
            comm.Parameters.Add("@StatementType", SqlDbType.VarChar, 50).Value = "Delete";
            comm.Parameters.Add("@StatementType", SqlDbType.VarChar, 50).Value = "Insert";
            comm.Parameters.Add("@StatementType", SqlDbType.VarChar, 50).Value = "Update";
            comm.Parameters.Add("@StatementType", SqlDbType.VarChar, 50).Value = "Select";

            //          @Emp_Id = N'14',
            //@Dept_id = N'22',
            //@Emp_Sal = N'222',
            //@Dept_Name = N'sdf',
            //@StatementType = N'Insert'

            comm.CommandType = CommandType.StoredProcedure;
            connection.Open();
            int x = comm.ExecuteNonQuery();
            connection.Close();
            if(x>0)
            {
                Response.Write("Inserted");
            }
            else
            {
                Response.Write("Not Inserted");
            }
        }
    }
}