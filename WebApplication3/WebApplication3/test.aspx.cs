﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            HttpCookie ck = new HttpCookie("mycookie");
            ck.Value = "Welcome cookie";
            Response.Cookies.Add(ck);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Session["my"] = "welcome session";
        }
    }
}