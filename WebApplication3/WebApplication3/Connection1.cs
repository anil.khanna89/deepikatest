﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication3
{
    public class Connection1
    {
        string ConnectionString = "";
        SqlConnection con;

        public void OpenConnection()
        {
            con = new SqlConnection(ConnectionString);
            con.Open();
        }

      

        public void ExecuteQuery(string Query_)
        {
            SqlCommand command = new SqlCommand(Query_, con);
            command.ExecuteNonQuery();
         
        }
        public SqlDataReader DataReader(string Query_)
        {
            SqlCommand command = new SqlCommand(Query_, con);
            SqlDataReader dr = command.ExecuteReader();
            return dr;
        }

    }

}