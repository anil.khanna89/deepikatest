﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class test2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["mycookie"] != null)
            {
                HttpCookie ck = Request.Cookies["mycookie"];
                Label1.Text += ck.Value;
            }

            if (Session["my"] != null)
            {
                Label1.Text += Session["my"].ToString();
            }
        }
    }
}