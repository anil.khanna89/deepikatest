﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace employees
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);

                connection.Open();
                SqlCommand insertcommand = new SqlCommand("Insert into Employee_Info(Emp_id,Emp_Sal,Dept_Name,Dept_Id) Values('" + txtemp_id.Text + "','" + txtemp_sal.Text + "','" + txtdept_name.Text + "','" + txtdept_id.Text + "')", connection);

                insertcommand.ExecuteNonQuery();
                lblMsg.Text += " Try Block ";
            }
            catch(Exception ex)
            {
                lblMsg.Text += " " + ex.Message;
            }
            finally
            {
                lblMsg.Text += " Finally Block ";
            }
            
        }
    }
}