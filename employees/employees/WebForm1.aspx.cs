﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace employees
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        
        protected void Button1_Click1(object sender, EventArgs e)
        {
            //@Dept_Id nvarchar(50)out,@Emp_Sal nvarchar(50)out,@Dept_Name nvarchar(50)out,@Emp_id nvarchar(50) 
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
            SqlCommand comm = new SqlCommand("GetDataByEmployeess", connection);
            comm.CommandType = CommandType.StoredProcedure;

            SqlParameter P_Emp_id = new SqlParameter("@Emp_id", SqlDbType.NVarChar, 50);
            P_Emp_id.Direction = ParameterDirection.Input;
            P_Emp_id.Value = txtemp_id.Text;

            SqlParameter P_Dept_Id = new SqlParameter("@Dept_Id", SqlDbType.NVarChar, 50);
            P_Dept_Id.Direction = ParameterDirection.Output;

            SqlParameter P_Dept_Name = new SqlParameter("@Dept_Name", SqlDbType.NVarChar, 50);
            P_Dept_Name.Direction = ParameterDirection.Output;

            SqlParameter P_Emp_Sal = new SqlParameter("@Emp_Sal", SqlDbType.NVarChar, 50);
            P_Emp_Sal.Direction = ParameterDirection.Output;

            //comm.Parameters.Add(parm);
            comm.Parameters.Add(P_Dept_Id);
            comm.Parameters.Add(P_Emp_Sal);
            comm.Parameters.Add(P_Dept_Name);
            comm.Parameters.Add(P_Emp_id);

            connection.Open();
            comm.ExecuteNonQuery();
            txtdept_id.Text = P_Dept_Id.Value.ToString();
            txtdept_name.Text = P_Dept_Name.Value.ToString();
            txtemp_sal.Text = P_Emp_Sal.Value.ToString();
            connection.Close();
        }
    }
}