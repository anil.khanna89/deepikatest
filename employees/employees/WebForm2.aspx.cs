﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace employees
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        void _Msg(string s)
        {
            lblMsg.Text = s;
            lblMsg.Visible = true;
        }

        void _Clear(string s)
        {

        }
        void Disp()
        {
            MyDB_Operations _Obj = new MyDB_Operations();
            DataTable _Dt = _Obj.GetTable("Select * from Employee_Info");
            if (_Dt != null)
            {
                GridView1.DataSource = _Dt;
                GridView1.DataBind();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Disp();
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {// Insert

            MyDB_Operations _Obj = new MyDB_Operations();
            string Qry = "insert into Employee_Info values('"+txtemp_id.Text+"','"+txtemp_name.Text+"','"+txtemp_sal.Text+"','"+txtdept_name.Text+"')";
            int i = _Obj.DML_Operations(Qry);
            if(i>0)
            {
                _Msg("Record Saved");
            }
            else
            {
                _Msg("Not Saved");
            }
            Disp();
        }

        protected void btnSaveWithSP_Click(object sender, EventArgs e)
        {/*
            @emp_id nvarchar(50),
@emp_sal nvarchar(50),
@dept_name nvarchar(50),
@emp_name nvarchar(50),
@StatementType nvarchar(50)=''
             */
            MyDB_Operations _Obj = new MyDB_Operations();
            SqlParameter[] sp = new SqlParameter[5];
            sp[0] = new SqlParameter("@emp_id", txtemp_id.Text);
            sp[1] = new SqlParameter("@emp_sal", txtemp_sal.Text);
            sp[2] = new SqlParameter("@dept_name", txtdept_name.Text);
            sp[3] = new SqlParameter("@emp_name", txtemp_name.Text);
            sp[4] = new SqlParameter("@StatementType", "Insert");

            int i = _Obj.DML_Operations_With_SP("InsertUpdateDelete1", sp);
            if (i > 0)
            {
                _Msg("Record Saved");
            }
            else
            {
                _Msg("Not Saved");
            }
            Disp();
        }

        protected void btndeletewithSP_Click(object sender, EventArgs e)
        {
            MyDB_Operations _Obj = new MyDB_Operations();
            SqlParameter[] sp = new SqlParameter[5];
            sp[0] = new SqlParameter("@emp_id", txtemp_id.Text);
            sp[1] = new SqlParameter("@emp_sal", txtemp_sal.Text);
            sp[2] = new SqlParameter("@dept_name", txtdept_name.Text);
            sp[3] = new SqlParameter("@emp_name", txtemp_name.Text);
            sp[4] = new SqlParameter("@StatementType", "Delete");
            int i = _Obj.DML_Operations_With_SP("InsertUpdateDelete1", sp);
            if (i > 0)
            {
                _Msg("Record deleted");
            }
            else
            {
                _Msg("Not deleted");
            }
            Disp();
        }

        protected void btnUpdatewithSP_Click(object sender, EventArgs e)
        {
            MyDB_Operations _Obj = new MyDB_Operations();
            SqlParameter[] sp = new SqlParameter[5];
            sp[0] = new SqlParameter("@emp_id", txtemp_id.Text);
            sp[1] = new SqlParameter("@emp_sal", txtemp_sal.Text);
            sp[2] = new SqlParameter("@dept_name", txtdept_name.Text);
            sp[3] = new SqlParameter("@emp_name", txtemp_name.Text);
            sp[4] = new SqlParameter("@StatementType", "Update");
            int i = _Obj.DML_Operations_With_SP("InsertUpdateDelete1", sp);
            if (i > 0)
            {
                _Msg("Record updated successfully");
            }
            else
            {
                _Msg("record not updated");
            }
            Disp();
        }


    }
}