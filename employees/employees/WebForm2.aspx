﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="employees.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" />
                    <asp:CommandField ShowEditButton="True" />
                </Columns>
            </asp:GridView>
        </div>

         <div>
        <table style="border:1px solid black;font-family:Arial">
            <tr>
                <td>
                    Employee Id
                </td>
                <td>
                    <asp:TextBox ID="txtemp_id" runat="server"></asp:TextBox>
                    <asp:Button ID="Button3" runat="server" Text="Find" />

                </td>
            </tr>
                <tr>
           
                <td>Employee salary</td>
                <td>
                    <asp:TextBox ID="txtemp_sal" runat="server"></asp:TextBox>
                    <asp:Button ID="Button4" runat="server" Text="Find" />
                </td>
               </tr>
                   <tr>
                    <td>
                    Employee Name
                </td>
                <td>
                    <asp:TextBox ID="txtemp_name" runat="server"></asp:TextBox>
                    <asp:Button ID="Button5" runat="server" Text="Find" />
                </td>
                       </tr>
            <tr>
                <td>
                    Department Name
                </td>
                <td>
                     <asp:TextBox ID="txtdept_name" runat="server"></asp:TextBox>
                    <asp:Button ID="Button6" runat="server" Text="Find" />
                </td>
                
            </tr>
            <tr>
                <td>
                      <asp:Button ID="Button1" runat="server" Text="Update" /> <br />
                     <br />
                     <br />
                </td>
                <td>
                     <asp:Button ID="Button2" runat="server" Text="Add New Record" OnClick="Button2_Click" />
                     <br />
                     <br />
                     <asp:Label ID="lblMsg" runat="server" style="font-weight: 700; color: #FF0000" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSaveWithSP" runat="server" Text="INSERT" OnClick="btnSaveWithSP_Click" />
                </td>

                <td>
                    <asp:Button ID="btndeletewithSP" runat="server" Text="DELETE" OnClick="btndeletewithSP_Click"/>
                </td>

                <td>
                    
                      <asp:Button ID="btnUpdatewithSP" runat="server" Text="UPDATE" OnClick="btnUpdatewithSP_Click"/>
                </td>
            </tr>
        </table>
       
          
             </div>

    </form>
</body>

</html>
