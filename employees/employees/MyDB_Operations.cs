﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace employees
{
    public class MyDB_Operations
    {
        SqlConnection _Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
        SqlCommand _Cmd;
        SqlDataAdapter _Da;
        DataSet _Ds;
        
        abstract class test
        {
           public abstract void testAbs1();
            public abstract void testAbs2();
        }
        interface myinterface
        {
            void test();
            void test2();
        }

        //class xx:test
        //{

        //}

        public int DML_Operations(string Qry)
        {
            int i = -1;
            try
            {
                using (_Cmd = new SqlCommand(Qry, _Conn))
                {
                    if (_Conn.State == ConnectionState.Closed)
                    {
                        _Conn.Open();
                    }
                    i = _Cmd.ExecuteNonQuery();
                    if (_Conn.State == ConnectionState.Open)
                    {
                        _Conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                i = -1;
            }
            finally
            {
                if (_Conn.State == ConnectionState.Open)
                {
                    _Conn.Close();
                }
            }
            return i;
        }

        public int DML_Operations_With_SP(string spName, SqlParameter[] sp)
        {
            int i = -1;
            try
            {
                if (sp != null)
                {
                    using (_Cmd = new SqlCommand(spName, _Conn))
                    {
                        if (_Conn.State == ConnectionState.Closed)
                        {
                            _Conn.Open();
                        }

                        // Add SqlParameters in SqlCommand Object
                        for (int x = 0; x < sp.Length; x++)
                        {
                            _Cmd.Parameters.Add(new SqlParameter(sp[x].ParameterName, sp[x].Value));
                        }

                        // Assign SqlCommand Type
                        _Cmd.CommandType = CommandType.StoredProcedure;

                        i = _Cmd.ExecuteNonQuery();
                        if (_Conn.State == ConnectionState.Open)
                        {
                            _Conn.Close();
                        }
                    }
                }
                else
                {
                    i = -1;
                }
            }
            catch (Exception ex)
            {
                i = -1;
            }
            finally
            {
                if (_Conn.State == ConnectionState.Open)
                {
                    _Conn.Close();
                }
            }
            return i;
        }

        public DataTable GetTable(string Qry)
        {
            DataTable _Dt = null;
            try
            {
                using (_Da = new SqlDataAdapter(Qry, _Conn))
                {
                    using (_Ds=new DataSet())
                    {
                        _Da.Fill(_Ds);
                        _Dt = _Ds.Tables[0];
                    }
                }
            }
            catch(Exception ex)
            {
                _Dt = null;
            }
            return _Dt;
        }
    }
}