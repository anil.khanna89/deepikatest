USE [master]
GO
/****** Object:  Database [Registration]    Script Date: 2/1/2019 4:55:51 PM ******/
CREATE DATABASE [Registration]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Registration', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Registration.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Registration_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Registration_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Registration] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Registration].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Registration] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Registration] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Registration] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Registration] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Registration] SET ARITHABORT OFF 
GO
ALTER DATABASE [Registration] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Registration] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Registration] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Registration] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Registration] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Registration] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Registration] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Registration] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Registration] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Registration] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Registration] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Registration] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Registration] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Registration] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Registration] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Registration] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Registration] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Registration] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Registration] SET  MULTI_USER 
GO
ALTER DATABASE [Registration] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Registration] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Registration] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Registration] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Registration] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Registration] SET QUERY_STORE = OFF
GO
USE [Registration]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Registration]
GO
/****** Object:  Table [dbo].[Department_Info]    Script Date: 2/1/2019 4:55:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department_Info](
	[Dept_Id] [nvarchar](50) NOT NULL,
	[Dept_Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [dept_Pk] PRIMARY KEY CLUSTERED 
(
	[Dept_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee_Duplicate]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Duplicate](
	[Emp_Id] [nvarchar](50) NULL,
	[Dept_Id] [nvarchar](50) NULL,
	[Emp_Sal] [nvarchar](50) NULL,
	[Dept_Name] [nvarchar](50) NULL,
	[Emp_Info] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee_Info]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Info](
	[Emp_Id] [nvarchar](50) NOT NULL,
	[Emp_Sal] [nvarchar](50) NULL,
	[Dept_Name] [nvarchar](50) NULL,
	[Emp_Name] [nvarchar](50) NULL,
 CONSTRAINT [employees_pk] PRIMARY KEY CLUSTERED 
(
	[Emp_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRegistration]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRegistration](
	[UserName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Phone-Number] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserRegistration] PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Employee_Duplicate] ([Emp_Id], [Dept_Id], [Emp_Sal], [Dept_Name], [Emp_Info]) VALUES (NULL, NULL, NULL, NULL, N'update record After Insert')
GO
INSERT [dbo].[Employee_Duplicate] ([Emp_Id], [Dept_Id], [Emp_Sal], [Dept_Name], [Emp_Info]) VALUES (N'10', N'101', N'58000', N'computer science', N'update record After Insert')
GO
INSERT [dbo].[Employee_Info] ([Emp_Id], [Emp_Sal], [Dept_Name], [Emp_Name]) VALUES (N'10', N'58000', N'computer science', NULL)
GO
INSERT [dbo].[Employee_Info] ([Emp_Id], [Emp_Sal], [Dept_Name], [Emp_Name]) VALUES (N'12', N'20000', N'Arts', NULL)
GO
INSERT [dbo].[Employee_Info] ([Emp_Id], [Emp_Sal], [Dept_Name], [Emp_Name]) VALUES (N'131', N'40000', N'cs', NULL)
GO
INSERT [dbo].[Employee_Info] ([Emp_Id], [Emp_Sal], [Dept_Name], [Emp_Name]) VALUES (N'133', N'30000', N'arts', N'deepika')
GO
INSERT [dbo].[Employee_Info] ([Emp_Id], [Emp_Sal], [Dept_Name], [Emp_Name]) VALUES (N'21', N'30000', N'arts', NULL)
GO
INSERT [dbo].[Employee_Info] ([Emp_Id], [Emp_Sal], [Dept_Name], [Emp_Name]) VALUES (N'3', N'100000', N'test dept', N'test ram')
GO
INSERT [dbo].[Employee_Info] ([Emp_Id], [Emp_Sal], [Dept_Name], [Emp_Name]) VALUES (N'5', N'100000', N'test dept', N'test ram')
GO
INSERT [dbo].[UserRegistration] ([UserName], [Email], [Phone-Number], [Password]) VALUES (N'', N'', N'', N'')
GO
INSERT [dbo].[UserRegistration] ([UserName], [Email], [Phone-Number], [Password]) VALUES (N'amayra', N'amayra@gmail.com', N'421567', N'aaa')
GO
INSERT [dbo].[UserRegistration] ([UserName], [Email], [Phone-Number], [Password]) VALUES (N'deepa', N'deepa@gmail.com', N'1234567', N'1234')
GO
INSERT [dbo].[UserRegistration] ([UserName], [Email], [Phone-Number], [Password]) VALUES (N'deepika', N'deepika@gmail.com', N'12345', N'1223456')
GO
INSERT [dbo].[UserRegistration] ([UserName], [Email], [Phone-Number], [Password]) VALUES (N'deepika', N'khanna', N'9899021768', N'12345')
GO
ALTER TABLE [dbo].[Department_Info]  WITH CHECK ADD FOREIGN KEY([Dept_Id])
REFERENCES [dbo].[Employee_Info] ([Emp_Id])
GO
/****** Object:  StoredProcedure [dbo].[CreateNewRow]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateNewRow]
   (
   @emp_name nvarchar(50),
@emp_id nvarchar(50),
@emp_sal nvarchar(50),
@dept_name nvarchar(50),
@dept_id nvarchar(50))
AS

INSERT INTO Employee_Info
VALUES ('@emp_id','@dept_id','@emp_sal','@dept_name','@emp_name' )

RETURN SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[GetDataByEmployee]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetDataByEmployee](@Dept_Id nvarchar(50),@Emp_Sal nvarchar(50),@Dept_Name nvarchar(50),@Emp_id nvarchar(50))
as
begin
select Emp_Sal,Dept_Id,Dept_Name from Employee_Info where Emp_Id=@Emp_id;
end
GO
/****** Object:  StoredProcedure [dbo].[GetDataByEmployeeinfo]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetDataByEmployeeinfo](@Dept_Id nvarchar(50),@Emp_Sal nvarchar(50),@Dept_Name nvarchar(50),@Emp_id nvarchar(50))
as
begin
select Emp_Sal,Dept_Id,Dept_Name from Employee_Info where Emp_Id=@Emp_id;
end
GO
/****** Object:  StoredProcedure [dbo].[GetDataByEmployees]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDataByEmployees]  
  
   @Dept_Id nvarchar(50)out,@Emp_Sal nvarchar(50)out,@Dept_Name nvarchar(50)out,@Emp_id nvarchar(50) 
  

AS  
BEGIN  
   SELECT @Dept_Id = Dept_Id, @Emp_Sal = Emp_Sal, @Dept_Name = Dept_Name
   FROM Employee_Info
   WHERE   @Emp_id= Emp_Id
END
GO
/****** Object:  StoredProcedure [dbo].[GetDataByEmployeess]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDataByEmployeess]  
( 
   @Dept_Id nvarchar(50)out,@Emp_Sal nvarchar(50)out,@Dept_Name nvarchar(50)out,@Emp_id nvarchar(50) 
 ) 

AS  
BEGIN  
   SELECT @Dept_Id = Dept_Id, @Emp_Sal = Emp_Sal, @Dept_Name = Dept_Name
   FROM Employee_Info
   WHERE   Emp_Id=@Emp_id
END
GO
/****** Object:  StoredProcedure [dbo].[Insertnewrow]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertnewrow]
   (
   @emp_name nvarchar(50),
@emp_id nvarchar(50),
@emp_sal nvarchar(50),
@dept_name nvarchar(50),
@dept_id nvarchar(50))
AS

INSERT INTO Employee_Info
VALUES ('@emp_id','@dept_id','@emp_sal','@dept_name','@emp_name' )

RETURN SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateDelete]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[InsertUpdateDelete]
(

@emp_id nvarchar(50),
@dept_id nvarchar(50),
@emp_sal nvarchar(50),
@dept_name nvarchar(50),
 @emp_name nvarchar(50),
@StatementType nvarchar(50)=''
)
AS  
BEGIN  
IF @StatementType = 'Insert'  
BEGIN  
insert into Employee_Info values (@emp_id,@dept_id,@emp_sal,@dept_name,@emp_name) 
END  
IF @StatementType = 'Select'  
BEGIN  
select * from Employee_Info 
END  
IF @StatementType = 'Update'  
BEGIN  
UPDATE Employee_Info SET  
Emp_Sal=@emp_sal,Dept_Id=@dept_id,Dept_Name=@dept_name,Emp_Name=@emp_name
WHERE  Emp_Id= @emp_id 
END  
end
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateDelete1]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[InsertUpdateDelete1]
(

@emp_id nvarchar(50),
@emp_sal nvarchar(50),
@dept_name nvarchar(50),
@emp_name nvarchar(50),
@StatementType nvarchar(50)=''
)
AS  
BEGIN  
IF @StatementType = 'Insert'  
BEGIN  
insert into Employee_Info values (@emp_id,@emp_sal,@dept_name,@emp_name) 
END  
IF @StatementType = 'Select'  
BEGIN  
select * from Employee_Info 
END  
IF @StatementType = 'Update'  
BEGIN  
UPDATE Employee_Info SET  
Emp_Sal=@emp_sal,Dept_Name=@dept_name,Emp_Name=@emp_name
WHERE  Emp_Id= @emp_id 
END 
if @StatementType='Delete'
BEGIN
Delete from Employee_Info where Emp_Id= @emp_id
END
 
end
GO
/****** Object:  StoredProcedure [dbo].[MasterInsertUpdateDelete]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MasterInsertUpdateDelete]  
(  
@Emp_Id nvarchar(50), 
@Dept_id nvarchar(50),
 @Emp_Sal nvarchar(50),
 @Dept_Name nvarchar(50),
 @StatementType nvarchar(20) = '' 
)

AS  
BEGIN  
IF @StatementType = 'Insert'  
BEGIN  
insert into Employee_Info values( @Emp_Id,@Dept_id,@Emp_Sal,@Dept_Name)  
END  
IF @StatementType = 'Select'  
BEGIN  
select * from Employee_Info
END  
IF @StatementType = 'Update'  
BEGIN  
UPDATE Employee_Info SET  
Emp_Id = @Emp_Id, Emp_Sal = @Emp_Sal, Dept_Id = @Dept_id,  
Dept_Name = @Dept_Name  
WHERE Emp_Id = @Emp_Id  
END  
else IF @StatementType = 'Delete'  
BEGIN  
DELETE FROM Employee_Info WHERE Emp_Id = @Emp_Id  
END  
end  
 
GO
/****** Object:  StoredProcedure [dbo].[Submit]    Script Date: 2/1/2019 4:55:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  Create Procedure [dbo].[Submit]
 (

 @username varchar(50),
 @email varchar(50),
 @phonenumber varchar(50),
 @password varchar(50)
 ) 
 as
 begin
 insert into UserRegistration VALUES (@username,@email,@phonenumber,@password)
 End
GO
USE [master]
GO
ALTER DATABASE [Registration] SET  READ_WRITE 
GO
