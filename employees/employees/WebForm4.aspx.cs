﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace employees
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Insert(string name, string country)
        {
            string constr = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO Employee_Info  VALUES (@Emp_id,@Emp_Sal,@Dept_Id,@Dept_Name)"))
                {
                    cmd.Parameters.AddWithValue("@Emp_id",  1);
                    cmd.Parameters.AddWithValue("@Emp_Sal", country);
                    cmd.Parameters.AddWithValue("@Dept_Id", country);
                    cmd.Parameters.AddWithValue("@Dept_Name", country);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

    }
}